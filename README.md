## Description

This is Node.js backend app based on Nest.js framework.
Before run application you need to execute database via docker. Command see below.
Then you can run app. App will running on `localhost:3000`.
For testing I composed Postman collection exported in `crud-test.postman_collection.json`.
Or you can execute e2e tests. (They are runable but not fully finished. Also unit tests are not finished.) 

## Installation

```bash
$ npm install
```

## Before run app

```bash
$ docker-compose up
```

## Running the app

```bash
# development
$ npm run start

```

## Test

```bash
# unit tests
$ npm run test

# e2e tests
$ npm run test:e2e

# test coverage
$ npm run test:cov
```