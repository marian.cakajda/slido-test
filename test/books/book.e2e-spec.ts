import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import * as request from 'supertest';
import { BookModule } from '../../src/books/book.module';
import { BookService } from '../../src/books/book.service';
import { Book } from '../../src/books/book.entity';
import { Author } from '../../src/authors/author.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AuthorsModule } from '../../src/authors/author.module';

describe('BookController (e2e)', () => {
  let app: INestApplication;
  let bookService: BookService;

  beforeAll(async () => {
    const module: TestingModule = await Test.createTestingModule({
      imports: [
        BookModule,
        AuthorsModule,
        TypeOrmModule.forRoot({
          type: 'postgres',
          host: 'localhost',
          port: 5432,
          username: 'test',
          password: 'test',
          database: 'test',
          entities: [Book, Author],
          synchronize: true,
        }),
      ],
    }).compile();

    app = module.createNestApplication();
    await app.init();
    // bookRepository = module.get<Repository<Book>>(getRepositoryToken(Book));
    bookService = module.get<BookService>(BookService);
  });

  it('/book (POST)', async () => {
    const res = await request(app.getHttpServer())
      .post('/book')
      .send({
        title: 'FirstBook_#test1',
        description: 'FirstDesc_#test1',
        authors: [
          {
            firstName: 'FirstName_#test1',
            lastName: 'LastName_#test1',
          },
          {
            firstName: 'John_#test1',
            lastName: 'Doe_#test1',
          },
        ],
      });
    expect(res.status).toEqual(201);
    // expect(res.text).toEqual("{\"id\":3,\"title\":\"FirstBook_#test1\",\"description\":\"FirstDesc_#test1\",\"authors\":[{\"id\":5,\"firstName\":\"Majo_#test1\",\"lastName\":\"Cakajda_#test1\"},{\"id\":6,\"firstName\":\"John_#test1\",\"lastName\":\"Doe_#test1\"}]}");
  });

  it('/book (GET)', async () => {
    const res = await request(app.getHttpServer())
      .get('/book')
      .set('Accept', 'application/json');
    expect(res.status).toEqual(200);
    // expect(res.text).toEqual("[{\"id\":2,\"title\":\"FirstBook_#7\",\"description\":\"FirstDesc_#7\",\"authors\":[{\"id\":1,\"firstName\":\"Majo_#2\",\"lastName\":\"Cakajda_#2\"},{\"id\":2,\"firstName\":\"John_#3\",\"lastName\":\"Doe_#3\"}]}]");
  });

  it('/book/findByTitle (GET)', async () => {
    const res = await request(app.getHttpServer())
      .get('/book/findByTitle?title=FirstBook_#test1')
      .set('Accept', 'application/json');
    expect(res.status).toEqual(200);
  });

  it('/book (PUT)', async () => {
    const res = await request(app.getHttpServer())
      .put('/book/3')
      .send({
        title: 'FirstBook_#test2',
        description: 'FirstDesc_#test2',
        authors: [
          {
            firstName: 'FirstName_#test1',
            lastName: 'LastName_#test1',
            id: 3,
          },
          {
            firstName: 'John_#test1',
            lastName: 'Doe_#test1',
            id: 4,
          },
        ],
      });
    expect(res.status).toEqual(200);
  });

  it('/book (DELETE)', async () => {
    const res = await request(app.getHttpServer()).delete('/book/3');
    expect(res.status).toEqual(200);
  });

  afterAll(async () => {
    await app.close();
  });
});
