export class AuthorDto {
  firstName: string;
  lastName: string;
}

type AuthorCollectionDto = AuthorDto[];

export class BookDto {
  title: string;
  description: string;
  authors: AuthorCollectionDto;
}
