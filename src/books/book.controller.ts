import {
  Controller,
  Get,
  Post,
  Body,
  Delete,
  Put,
  Param,
  Query,
} from '@nestjs/common';
import { Book } from './book.entity';
import { BookService } from './book.service';
import { BookDto } from './dto/book.dto';

@Controller('book')
export class BookController {
  constructor(private readonly userService: BookService) {}

  @Post()
  async create(@Body() bookDto: BookDto): Promise<Book> {
    return await this.userService.create(bookDto);
  }

  @Get()
  async findAll(): Promise<Book[]> {
    return await this.userService.findAll();
  }

  @Get('findByTitle')
  async findByTitle(@Query('title') title: string): Promise<Book> {
    return await this.userService.findByTitle(title);
  }

  @Put(':id')
  async update(@Param('id') id: number, @Body() bookDto: BookDto) {
    return await this.userService.update(id, bookDto);
  }

  @Delete(':id')
  async delete(@Param('id') id: number): Promise<any> {
    return await this.userService.delete(id);
  }
}
