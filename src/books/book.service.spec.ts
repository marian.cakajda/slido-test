import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { Book } from './book.entity';
import { Repository } from 'typeorm';
import { BookService } from './book.service';
import { AuthorService } from '../authors/author.service';
import { Author } from '../authors/author.entity';
import { bookArray, oneBook } from '../utils/mockData';

describe('BookService', () => {
  let bookService: BookService;
  let bookRepository: Repository<Book>;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        BookService,
        {
          provide: getRepositoryToken(Book),
          useValue: {
            findAll: jest.fn().mockResolvedValue(bookArray),
            findOne: jest.fn().mockResolvedValue(oneBook),
            create: jest.fn().mockResolvedValue(oneBook),
            remove: jest.fn(),
            delete: jest.fn(),
          },
        },
        AuthorService,
        {
          provide: getRepositoryToken(Author),
          useValue: {},
        },
      ],
    }).compile();

    bookService = module.get<BookService>(BookService);
    bookRepository = module.get<Repository<Book>>(getRepositoryToken(Book));
  });

  it('should be defined', () => {
    expect(bookService).toBeDefined();
  });
});
