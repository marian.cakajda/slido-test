import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, getConnection } from 'typeorm';
import { Book } from './book.entity';
import { BookDto } from './dto/book.dto';
import { Author } from '../authors/author.entity';

@Injectable()
export class BookService {
  constructor(
    @InjectRepository(Book)
    private bookRepository: Repository<Book>,
    @InjectRepository(Author)
    private authorRepository: Repository<Author>,
  ) {}

  async findAll(): Promise<Book[]> {
    return await this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'author')
      .getMany();
  }

  async findByTitle(title: string): Promise<Book> {
    return await this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'author')
      .where('book.title = :title', { title })
      .getOne();
  }

  async create(bookDto: BookDto): Promise<Book> {
    const savedBook: Book = await this.bookRepository
      .createQueryBuilder('book')
      .leftJoinAndSelect('book.authors', 'author')
      .where('book.title = :title', { title: bookDto.title })
      .getOne();

    if (savedBook != undefined && bookDto.title == savedBook.title) {
      return savedBook;
    }

    const book = new Book();
    book.title = bookDto.title;
    book.description = bookDto.description;
    book.authors = new Array<Author>();

    for (const item of bookDto.authors) {
      const author = new Author();
      const savedAuthor: Author = await this.authorRepository
        .createQueryBuilder('author')
        .where(
          'author.firstName = :firstName AND author.lastName = :lastName',
          { firstName: item.firstName, lastName: item.lastName },
        )
        .getOne();

      if (
        savedAuthor != undefined &&
        item.firstName == savedAuthor.firstName &&
        item.lastName == savedAuthor.lastName
      ) {
        book.authors.push(savedAuthor);
      } else {
        author.firstName = item.firstName;
        author.lastName = item.lastName;
        await this.authorRepository.save(author);
        book.authors.push(author);
      }
    }
    return await this.bookRepository.save(book);
  }

  async update(id: number, bookDto: BookDto): Promise<Book> {
    const bookUpdated = await this.bookRepository.findOne(id);
    const dataUpdated = {
      ...bookUpdated,
      title: bookDto.title,
      description: bookDto.description,
      authors: bookDto.authors,
    };
    return await this.bookRepository.save(dataUpdated);
  }

  async delete(id: number): Promise<any> {
    await getConnection()
      .createQueryBuilder()
      .delete()
      .from(Book)
      .where('id = :id', { id: id })
      .execute();
  }
}
