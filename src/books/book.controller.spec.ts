import { Test, TestingModule } from '@nestjs/testing';
import { BookController } from './book.controller';
import { BookService } from './book.service';
import { bookArray } from '../utils/mockData';

describe('BookController', () => {
  let bookController: BookController;
  let bookService: BookService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [BookController],
      providers: [
        BookService,
        {
          provide: BookService,
          useValue: {
            findAll: jest.fn().mockResolvedValue(bookArray),
          },
        },
      ],
    }).compile();

    bookController = module.get<BookController>(BookController);
    bookService = module.get<BookService>(BookService);
  });

  it('should be defined', () => {
    expect(BookController).toBeDefined();
  });

  describe('findAll()', () => {
    it('should gets all users ', () => {
      bookController.findAll();
      expect(bookService.findAll).toHaveBeenCalled();
    });
  });
});
