export const bookArray = [
  {
    id: 1,
    title: 'Moby Dick',
    description: 'Moby Dick Desc',
    authors: [
      {
        id: 1,
        firstName: 'Herman',
        lastName: 'Melville',
      },
      {
        id: 2,
        firstName: 'John',
        lastName: 'Doe',
      },
    ],
  },
  {
    id: 2,
    title: 'Invisible Man',
    description: 'Invisible Man Desc',
    authors: [
      {
        id: 3,
        firstName: 'William',
        lastName: 'Faulkner',
      },
      {
        id: 4,
        firstName: 'Jane',
        lastName: 'Doe',
      },
    ],
  },
];

export const oneBook = {
  id: 1,
  title: 'Moby Dick',
  description: 'Moby Dick Desc',
  authors: [
    {
      id: 1,
      firstName: 'Herman',
      lastName: 'Melville',
    },
    {
      id: 2,
      firstName: 'John',
      lastName: 'Doe',
    },
  ],
};
