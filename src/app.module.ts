import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';

import { Connection } from 'typeorm';
import { BookModule } from './books/book.module';
import { AuthorsModule } from './authors/author.module';
import { Book } from './books/book.entity';
import { Author } from './authors/author.entity';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'postgres',
      host: 'localhost',
      port: 5432,
      username: 'test',
      password: 'test',
      database: 'test',
      entities: [Book, Author],
      synchronize: true,
    }),
    BookModule,
    AuthorsModule,
  ],
})
export class AppModule {
  constructor(private connection: Connection) {}
}
